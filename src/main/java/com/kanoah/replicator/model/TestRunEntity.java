package com.kanoah.replicator.model;

import net.java.ao.Entity;
import net.java.ao.OneToMany;
import net.java.ao.schema.Table;

@Table("TestRun")
public interface TestRunEntity extends Entity {
    @OneToMany(reverse = "getTestRun")
    TestResultEntity[] getTestResults();
}
