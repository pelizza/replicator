package com.kanoah.replicator.model;

import net.java.ao.Entity;
import net.java.ao.schema.Table;

@Table("TestResult")
public interface TestResultEntity extends Entity {
    TestRunEntity getTestRun();

    void setTestRun(TestRunEntity testRun);
}