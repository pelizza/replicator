package ut.com.kanoah.replicator;

import com.kanoah.replicator.model.TestResultEntity;
import com.kanoah.replicator.model.TestRunEntity;
import net.java.ao.EntityManager;
import net.java.ao.test.jdbc.DatabaseUpdater;

public class AODatabaseUpdater implements DatabaseUpdater {
    @Override
    public void update(EntityManager entityManager) throws Exception {
        entityManager.migrate(TestResultEntity.class);
        entityManager.migrate(TestRunEntity.class);
        entityManager.flushAll();
    }
}
