package ut.com.kanoah.replicator;

import com.atlassian.activeobjects.test.TestActiveObjects;
import com.atlassian.sal.api.user.UserKey;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.sal.api.user.UserProfile;
import com.kanoah.replicator.model.TestResultEntity;
import com.kanoah.replicator.model.TestRunEntity;
import net.java.ao.EntityManager;
import net.java.ao.test.jdbc.Data;
import net.java.ao.test.junit.ActiveObjectsJUnitRunner;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(ActiveObjectsJUnitRunner.class)
@Data(AODatabaseUpdater.class)
public class LinksDataAOServiceImplTest {
    private EntityManager entityManager;

    private TestActiveObjects ao;

    @Before
    public void setUp() throws Exception {
        ao = new TestActiveObjects(entityManager);
    }

    @Test
    public void aoBug() throws Exception {
        final TestRunEntity testRunEntity = ao.create(TestRunEntity.class);
        TestResultEntity[] testResults = testRunEntity.getTestResults();
        assertThat(testResults.length, is(equalTo(0)));

        final TestResultEntity testResultEntity = ao.create(TestResultEntity.class);
        testResultEntity.setTestRun(testRunEntity);
        testResultEntity.save();

        testResults = testRunEntity.getTestResults();
        assertThat(testResults.length, is(equalTo(1)));
    }
}
